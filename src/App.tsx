import React, { FunctionComponent } from 'react'
import MockNorrisApi from './api/mock-norris-api'
import './App.css'
import MockNorris from './MockNorris'

const App: FunctionComponent = () => {
    // const api = mockApi(new MockNorrisApi())
    const api = new MockNorrisApi()

    return <MockNorris api={api} />
}

export default App
