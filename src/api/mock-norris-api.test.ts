import MockAdapter from 'axios-mock-adapter'
import MockNorrisApi, { MockNorrisEndpoints } from './mock-norris-api'
import json from './mock/jokes/random.json'

it('should retry 3 times', async () => {
    expect.assertions(1)

    const api = new MockNorrisApi()
    const adapter = new MockAdapter(api.axios)

    adapter.onGet(MockNorrisEndpoints.Jokes.Random).replyOnce(500)
    adapter.onGet(MockNorrisEndpoints.Jokes.Random).replyOnce(500)
    adapter.onGet(MockNorrisEndpoints.Jokes.Random).replyOnce(500)
    adapter.onGet(MockNorrisEndpoints.Jokes.Random).replyOnce(200, json)

    const response = await api.fetchRandomJoke()

    expect(response).toBeDefined()
})
