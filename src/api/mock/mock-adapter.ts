import MockAdapter from 'axios-mock-adapter'
import MockNorrisApi, { MockNorrisEndpoints } from '../mock-norris-api'

export function mockApi(api: MockNorrisApi) {
    const adapter = new MockAdapter(api.axios, { delayResponse: 1000 })

    adapter.onGet(MockNorrisEndpoints.Jokes.Random).reply(500)

    return api
}
