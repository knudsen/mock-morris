export default function replaceChuck(text: string) {
    return text.replace(/chuck/gi, 'Mock')
}
