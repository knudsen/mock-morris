import React, { FunctionComponent, useCallback, useEffect, useState } from 'react'
import { Joke } from './api/api-types'
import MockNorrisApi from './api/mock-norris-api'
import './App.css'
import logo from './logo.png'
import replaceChuck from './util/replace-chuck'

interface Props {
    api: MockNorrisApi
}

const MockNorris: FunctionComponent<Props> = ({ api }) => {
    const [joke, setJoke] = useState<Joke>()
    const [fetching, setFetching] = useState<boolean>(true)
    const [error, setError] = useState<Error>()

    const fetchJoke = useCallback(async () => {
        try {
            setFetching(true)
            const joke = await api.fetchRandomJoke()
            setFetching(false)
            setJoke(joke)
        } catch (error) {
            setError(error)
        }
    }, [api])

    useEffect(() => {
        fetchJoke()
    }, [api, fetchJoke])

    const text = error ? `Noo! ${error.message}` : fetching ? 'Fetching...' : joke ? replaceChuck(joke.value) : ''

    return (
        <div className="App">
            <header className="App-header">
                <img src={logo} className="App-logo" alt="logo" onClick={fetchJoke} />
                <p>{text}</p>
            </header>
        </div>
    )
}

export default MockNorris
