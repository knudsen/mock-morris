import Axios, { AxiosInstance } from 'axios'
import { Joke } from './api-types'

const BASE_URL = 'https://api.chucknorris.io'
const NUMBER_OF_RETRIES = 3

export const MockNorrisEndpoints = {
    Jokes: {
        Random: '/jokes/random'
    }
}



class Request<T> {
    private retries = NUMBER_OF_RETRIES

    constructor(private axios: AxiosInstance) {}

    async perform(endpoint: string): Promise<T> {
        try {
            const response = await this.axios.get(endpoint)

            return response.data
        } catch (error) {
            if (this.retries > 0) {
                this.retries--
                return this.perform(endpoint)
            } else {
                throw error
            }
        }
    }
}

/**
 * @see https://api.chucknorris.io/
 */
class MockNorrisApi {
    readonly axios: AxiosInstance

    constructor() {
        this.axios = Axios.create({ baseURL: BASE_URL })
    }

    async fetchRandomJoke() {
        const request = new Request<Joke>(this.axios)
        return request.perform(MockNorrisEndpoints.Jokes.Random)
    }
}

export default MockNorrisApi
